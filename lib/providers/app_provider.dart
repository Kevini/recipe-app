import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';
import 'package:recipe_app/models/category_model.dart';
import 'package:recipe_app/utils/constants.dart';


class AppProvider extends ChangeNotifier {

  final List<CategoryModel> _categories = [
    CategoryModel('Noodles', imageUrl:url),
    CategoryModel('Beef', imageUrl: url),
    CategoryModel('Mandazi', imageUrl: url),
    CategoryModel('Rice', imageUrl: url),
    CategoryModel('Chapos', imageUrl: url)
    ];


  List<CategoryModel> get categories => _categories;


  void addCategory(CategoryModel newCategory){
    _categories.add(newCategory);

    notifyListeners();
  }
}
