import 'package:flutter/material.dart';
import 'package:recipe_app/utils/constants.dart';

class WaveBorderCard extends StatelessWidget {
  WaveBorderCard({this.recipeImageUrl = url, this.isMobileScreen = true, this.title = '', this.width = 350.0,});

  final String recipeImageUrl;
  final bool isMobileScreen;
  final String title;
  final double width;


  @override
  Widget build(BuildContext context) {
    return Stack(alignment: Alignment.bottomLeft, children: [
      Container(
        height: 200,
        width: width,
        decoration: BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Color(0X33000000),
              offset: Offset(3, 3),
              blurRadius: (0.0),
            )
          ],
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(20.0),
              bottomRight: Radius.circular(20.0)),
          image: DecorationImage(
            fit: BoxFit.cover,
            image: NetworkImage(recipeImageUrl),
          ),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(
            bottom: isMobileScreen ? 8.0 : 16.0, left: isMobileScreen ? 8 : 12),
        child: Container(
          padding: EdgeInsets.only(left: isMobileScreen ? 8 : 12),
          alignment: Alignment.centerLeft,
          height: isMobileScreen ? 40 : 100,
          width: width - 40,
          decoration: BoxDecoration(
              color: Colors.white.withOpacity(0.8),
              borderRadius:
                  BorderRadius.only(bottomRight: Radius.circular(50))),
          child: Hero(
            tag: title,
                      child: Text(
              title,
              style: TextStyle(fontSize: 16.0),
            ),
          ),
        ),
      ),
    ]);
  }
}
