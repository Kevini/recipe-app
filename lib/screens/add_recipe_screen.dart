import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:recipe_app/providers/app_provider.dart';

class AddRecipeScreen extends StatefulWidget {
  @override
  _AddRecipeScreenState createState() => _AddRecipeScreenState();
}

class _AddRecipeScreenState extends State<AddRecipeScreen> {
  final _formKey = GlobalKey<FormState>();
  String dropdownValue;
  File _image;

  @override
  void initState() {
    dropdownValue =
        Provider.of<AppProvider>(context, listen: false).categories[0].name;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Add Recipe'),
      ),
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage("assets/images/food.jpg"),
                fit: BoxFit.cover)),
        child: Padding(
          padding: EdgeInsets.only(
            top: 8,
            left: 8,
          ),
          child: Stack(children: [
            Positioned(
              bottom: -16,
              right: -16,
              child: Container(
                height: MediaQuery.of(context).size.height * 0.8,
                width: MediaQuery.of(context).size.width * 0.8,
                decoration: BoxDecoration(
                    border: Border.all(
                      color: Color(0xFF707070),
                      width: 3,
                    ),
                    boxShadow: [
                      BoxShadow(
                          color: Colors.black12,
                          blurRadius: 10,
                          offset: Offset(-2, -2))
                    ],
                    color: Colors.white.withOpacity(0.8),
                    borderRadius:
                        BorderRadius.only(topLeft: Radius.circular(120))),
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 16, right: 16),
              child: Form(
                key: _formKey,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.only(left: 28),
                      alignment: Alignment.center,
                      height: 48,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(25)),
                      child: TextFormField(
                        decoration: InputDecoration.collapsed(
                          hintText: 'Recipe Name',
                        ),
                      ),
                    ),
                    // SizedBox(
                    //   height: 20,
                    // ),
                    Consumer<AppProvider>(
                      builder: (context, appProvider, child) {
                        return Container(
                          padding: EdgeInsets.only(left: 28),
                          alignment: Alignment.center,
                          height: 48,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(25)),
                          child: DropdownButtonFormField(
                              decoration:
                                  InputDecoration.collapsed(hintText: null),
                              value: dropdownValue,
                              items: appProvider.categories.map((category) {
                                return DropdownMenuItem<String>(
                                    value: category.name,
                                    child: Text(category.name));
                              }).toList(),
                              onChanged: (selectedCategoryName) {
                                setState(() {
                                  dropdownValue = selectedCategoryName;
                                });
                              }),
                        );
                      },
                    ),

                    Container(
                      height: 180,
                      width: 300,
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(25)),
                      child: _image == null
                          ? IconButton(
                              icon: Icon(
                                Icons.camera,
                                size: 48,
                              ),
                              onPressed: () async {
                                // ignore: deprecated_member_use
                                _image = await ImagePicker.pickImage(
                                    source: ImageSource.gallery);
                                setState(() {});
                              },
                            )
                          : Image.file(_image),
                    ),
                    // SizedBox(
                    //   height: 20,
                    // ),
                    // SizedBox(
                    //   height: 16,
                    // ),
                    FlatButton(
                        color: Colors.lightBlueAccent,
                        onPressed: () {},
                        child: Text(
                          'Submit',
                          style: TextStyle(color: Colors.white),
                        ))
                  ],
                ),
              ),
            ),
          ]),
        ),
      ),
    );
  }
}
