import 'package:flutter/material.dart';


class CategoryScreen extends StatelessWidget {
  
  final String categoryName;

  CategoryScreen({this.categoryName});


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(categoryName),),
      body: Container(
        child: Center(child: Text(categoryName),),
      ),
    );
  }
}