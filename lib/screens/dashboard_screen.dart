import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:recipe_app/providers/app_provider.dart';
import 'package:recipe_app/screens/add_recipe_screen.dart';
import 'package:recipe_app/screens/category_screen.dart';
import 'package:recipe_app/utils/responsive_layout.dart';
import 'package:recipe_app/widgets/wave_border_card.dart';

class DashboardScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var isMobileScreen = ResponsiveLayout.isSmallScreen(context);
    var recipeImageUrl = 'https://loremflickr.com/300/240/food';

return Scaffold(
      backgroundColor: Color(0XFFEFECE7),
      body: Container(
        child: CustomScrollView(slivers: <Widget>[
          SliverAppBar(
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.only(right: 16.0),
                child: Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.green,
                  ),
                  child: IconButton(
                      icon: Icon(
                        Icons.add,
                        size: 30,
                        color: Colors.white,
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ChangeNotifierProvider(
                                  create: (BuildContext context) =>
                                      AppProvider(),
                                  child: AddRecipeScreen())),
                        );
                      }),
                ),
              )
            ],
            backgroundColor: Colors.transparent,
            expandedHeight: 250.0,
            flexibleSpace: FlexibleSpaceBar(
              background: Image(
                image: NetworkImage('https://loremflickr.com/320/240/food'),
                fit: BoxFit.cover,
              ),
            ),
          ),
          SliverPadding(
            padding: EdgeInsets.only(
                top: isMobileScreen ? 40.0 : 50.0,
                bottom: isMobileScreen ? 40.0 : 50.0,
                left: isMobileScreen ? 20.0 : 30.0),
            sliver: SliverList(
              delegate: SliverChildListDelegate([
                Text(
                  'Recipe of the Day',
                  style: TextStyle(fontSize: 20.0),
                ),
                SizedBox(height: 20),
                WaveBorderCard(
                  recipeImageUrl: recipeImageUrl,
                  isMobileScreen: isMobileScreen,
                  title: 'Pancakes',
                ),
                SizedBox(height: 20),
                Text(
                  'Categories',
                  style: Theme.of(context).textTheme.headline5,
                ),
                SizedBox(height: 20),
                ConstrainedBox(
                  constraints: BoxConstraints(maxHeight: 200),
                  child: Consumer<AppProvider>(
                    builder: (context, appProvider, child) {
                      return ListView.separated(
                        itemBuilder: (context, index) => GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              PageRouteBuilder(
                                  pageBuilder:
                                      (context, animationOne, animationTwo) =>
                                          CategoryScreen(
                                              categoryName: appProvider
                                                  .categories[index].name),
                                  transitionsBuilder: (context, animationOne,
                                      animationTwo, child) {
                                    var begin = Offset(1.0, 0);
                                    var end = Offset.zero;
                                    var curve = Curves.easeInOut;

                                    var tween = Tween(begin: begin, end: end)
                                        .chain(CurveTween(curve: curve));

                                    return SlideTransition(
                                        position: animationOne.drive(tween),
                                        child: child);
                                  },
                                  transitionDuration: Duration(seconds: 2)),
                            );
                          },
                          child: WaveBorderCard(
                            title: appProvider.categories[index].name,
                            width: 200,
                          ),
                        ),
                        separatorBuilder: (context, index) =>
                            SizedBox(width: 20),
                        itemCount: appProvider.categories.length,
                        scrollDirection: Axis.horizontal,
                      );
                    },
                  ),
                )
              ]),
            ),
          )
        ]),
      ),
    );
  }
}
